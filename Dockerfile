FROM php:7.2-fpm-alpine

# ******************************************
# General
# ******************************************

# Install packages
RUN apk add --no-cache \
    bash \
    nano \
    sed \
    curl \
    supervisor \
    perl

# Expose environment variables
ENV SP_APP_NAME="undefined" \
    SP_UPLOAD_SIZE="50M" \
    SP_INSTALL_WP=false

# Create default folders
RUN mkdir -p /var/www/html
RUN mkdir -p /var/www/logs

# Expose volume
VOLUME /var/www/html

# Set workdir
WORKDIR /var/www/html

# ******************************************
# PHP
# ******************************************

# Install php extensions
# Refference https://github.com/docker-library/wordpress/blob/master/php7.2/fpm-alpine/Dockerfile
RUN set -ex; \
	\
	apk add --no-cache --virtual .build-deps \
		libjpeg-turbo-dev \
		libpng-dev \
		libmemcached-dev \
	; \
	\
	curl -L -o /tmp/memcached.tar.gz "https://github.com/php-memcached-dev/php-memcached/archive/php7.tar.gz"; \
	mkdir -p /usr/src/php/ext/memcached; \
    tar -C /usr/src/php/ext/memcached -zxvf /tmp/memcached.tar.gz --strip 1; \
    rm /tmp/memcached.tar.gz; \
	\
	docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr; \
	docker-php-ext-configure memcached; \
	docker-php-ext-install gd mysqli opcache zip memcached; \
	\
	runDeps="$( \
		scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
			| tr ',' '\n' \
			| sort -u \
			| awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
	)"; \
	apk add --virtual .wordpress-phpexts-rundeps $runDeps; \
	apk del .build-deps

# Set recommended PHP.ini settings
# Refference https://secure.php.net/manual/en/opcache.installation.php
RUN { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=2'; \
		echo 'opcache.fast_shutdown=1'; \
		echo 'opcache.enable_cli=1'; \
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini

# Sitepilot config
COPY /conf/php-sitepilot.ini /opt/sitepilot/php-sitepilot.ini
COPY /conf/php-pool.conf /usr/local/etc/php-fpm.conf

# Expose port
EXPOSE 9000

# ******************************************
# Openresty
# ******************************************

# Docker build arguments
# Refference https://github.com/openresty/docker-openresty/tree/master/alpine
ARG RESTY_VERSION="1.13.6.2"
ARG RESTY_OPENSSL_VERSION="1.0.2p"
ARG RESTY_PCRE_VERSION="8.42"
ARG RESTY_J="1"
ARG RESTY_CONFIG_OPTIONS="\
    --with-file-aio \
    --with-http_addition_module \
    --with-http_auth_request_module \
    --with-http_dav_module \
    --with-http_flv_module \
    --with-http_geoip_module=dynamic \
    --with-http_gunzip_module \
    --with-http_gzip_static_module \
    --with-http_image_filter_module=dynamic \
    --with-http_mp4_module \
    --with-http_random_index_module \
    --with-http_realip_module \
    --with-http_secure_link_module \
    --with-http_slice_module \
    --with-http_ssl_module \
    --with-http_stub_status_module \
    --with-http_sub_module \
    --with-http_v2_module \
    --with-http_xslt_module=dynamic \
    --with-ipv6 \
    --with-mail \
    --with-mail_ssl_module \
    --with-md5-asm \
    --with-pcre-jit \
    --with-sha1-asm \
    --with-stream \
    --with-stream_ssl_module \
    --with-threads \
    --add-module=/tmp/ngx_cache_purge-2.3 \
    "
ARG RESTY_CONFIG_OPTIONS_MORE=""
ARG RESTY_ADD_PACKAGE_BUILDDEPS=""
ARG RESTY_ADD_PACKAGE_RUNDEPS=""
ARG RESTY_EVAL_PRE_CONFIGURE=""
ARG RESTY_EVAL_POST_MAKE=""
ARG _RESTY_CONFIG_DEPS="--with-openssl=/tmp/openssl-${RESTY_OPENSSL_VERSION} --with-pcre=/tmp/pcre-${RESTY_PCRE_VERSION}"

RUN apk add --no-cache --virtual .build-deps \
        build-base \
        curl \
        gd-dev \
        geoip-dev \
        libxslt-dev \
        linux-headers \
        make \
        perl-dev \
        readline-dev \
        zlib-dev \
        ${RESTY_ADD_PACKAGE_BUILDDEPS} \
    && apk add --no-cache \
        gd \
        geoip \
        libgcc \
        libxslt \
        zlib \
        ${RESTY_ADD_PACKAGE_RUNDEPS} \
    && cd /tmp \
    && curl -o ngx_cache_purge-2.3.tar.gz -fsSL https://github.com/FRiCKLE/ngx_cache_purge/archive/2.3.tar.gz \
    && tar xzvf ngx_cache_purge-2.3.tar.gz \
    && if [ -n "${RESTY_EVAL_PRE_CONFIGURE}" ]; then eval $(echo ${RESTY_EVAL_PRE_CONFIGURE}); fi \
    && curl -fSL https://www.openssl.org/source/openssl-${RESTY_OPENSSL_VERSION}.tar.gz -o openssl-${RESTY_OPENSSL_VERSION}.tar.gz \
    && tar xzf openssl-${RESTY_OPENSSL_VERSION}.tar.gz \
    && curl -fSL https://ftp.pcre.org/pub/pcre/pcre-${RESTY_PCRE_VERSION}.tar.gz -o pcre-${RESTY_PCRE_VERSION}.tar.gz \
    && tar xzf pcre-${RESTY_PCRE_VERSION}.tar.gz \
    && curl -fSL https://openresty.org/download/openresty-${RESTY_VERSION}.tar.gz -o openresty-${RESTY_VERSION}.tar.gz \
    && tar xzf openresty-${RESTY_VERSION}.tar.gz \
    && cd /tmp/openresty-${RESTY_VERSION} \
    && ./configure -j${RESTY_J} ${_RESTY_CONFIG_DEPS} ${RESTY_CONFIG_OPTIONS} ${RESTY_CONFIG_OPTIONS_MORE} \
    && make -j${RESTY_J} \
    && make -j${RESTY_J} install \
    && cd /tmp \
    && if [ -n "${RESTY_EVAL_POST_MAKE}" ]; then eval $(echo ${RESTY_EVAL_POST_MAKE}); fi \
    && rm -rf \
        openssl-${RESTY_OPENSSL_VERSION} \
        openssl-${RESTY_OPENSSL_VERSION}.tar.gz \
        openresty-${RESTY_VERSION}.tar.gz openresty-${RESTY_VERSION} \
        pcre-${RESTY_PCRE_VERSION}.tar.gz pcre-${RESTY_PCRE_VERSION} \
    && apk del .build-deps \
    && ln -sf /dev/stdout /usr/local/openresty/nginx/logs/access.log \
    && ln -sf /dev/stderr /usr/local/openresty/nginx/logs/error.log

# Add additional binaries into PATH for convenience
ENV PATH=$PATH:/usr/local/openresty/luajit/bin:/usr/local/openresty/nginx/sbin:/usr/local/openresty/bin

# Copy nginx configuration files
COPY /conf/nginx-cache.conf /usr/local/openresty/nginx/conf/nginx-cache.conf
COPY /conf/nginx-cache-skip.conf /usr/local/openresty/nginx/conf/nginx-cache-skip.conf
COPY /conf/nginx-expires.conf /usr/local/openresty/nginx/conf/nginx-expires.conf
COPY /conf/nginx-security.conf /usr/local/openresty/nginx/conf/nginx-security.conf
COPY /conf/nginx-wp.conf /usr/local/openresty/nginx/conf/nginx-wp.conf
COPY /conf/nginx-fastcgi-params.conf /usr/local/openresty/nginx/conf/fastcgi_params

# Generated in docker-entrypoint.sh
COPY /conf/nginx.conf /opt/sitepilot/nginx.conf
COPY /conf/nginx.vh.default.conf /opt/sitepilot/default.conf
COPY /conf/sp-config.php /opt/sitepilot/sp-config.php

# Expose port
EXPOSE 80

# ******************************************
# Supervisor
# ******************************************

ADD /conf/supervisord.conf /etc/supervisord.conf
CMD ["supervisord", "--nodaemon", "--configuration", "/etc/supervisord.conf"]

# ******************************************
# WPCLI
# ******************************************
RUN curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar && \
    chmod +x ./wp-cli.phar && \
    mv ./wp-cli.phar /usr/bin/wp

RUN wp cli version --allow-root

# ******************************************
# Composer
# ******************************************
RUN apk add --no-cache composer
RUN composer -v

# Add entrypoint script
COPY ./docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]

# Use SIGQUIT instead of default SIGTERM to cleanly drain requests
# See https://github.com/openresty/docker-openresty/blob/master/README.md#tips--pitfalls
STOPSIGNAL SIGQUIT