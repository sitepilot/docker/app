#!/bin/sh
log() {
    echo "[Sitepilot] $1"
}

echo ""
echo ""
echo "***************************************************"
echo "* Sitepilot App Container"
echo "* "
echo "* Environment:"
echo "* - SP_APP_NAME: $SP_APP_NAME"
echo "* - SP_WP_INSTALL: $SP_WP_INSTALL"
echo "* - SP_UPLOAD_SIZE: $SP_UPLOAD_SIZE"
echo "* "
echo "* Support: https://sitepilot.io"
echo "***************************************************"
echo ""
echo ""

log "Setup directories..."
mkdir -p /var/www/logs
mkdir -p /var/www/html
mkdir -p /etc/nginx/conf.d

log "Setup nginx config..."
cat  /opt/sitepilot/nginx.conf  | perl -p -e 's/\$\{([^}]+)\}/defined $ENV{$1} ? $ENV{$1} : $&/eg' > /usr/local/openresty/nginx/conf/nginx.conf

log "Setup virtual host..."
cat  /opt/sitepilot/default.conf  | perl -p -e 's/\$\{([^}]+)\}/defined $ENV{$1} ? $ENV{$1} : $&/eg' > /etc/nginx/conf.d/default.conf

log "Creating php config..."
cat  /opt/sitepilot/sp-config.php  | perl -p -e 's/\$\{([^}]+)\}/defined $ENV{$1} ? $ENV{$1} : $&/eg' > /var/www/sp-config.php
cat  /opt/sitepilot/php-sitepilot.ini  | perl -p -e 's/\$\{([^}]+)\}/defined $ENV{$1} ? $ENV{$1} : $&/eg' > /usr/local/etc/php/conf.d/00-sitepilot.ini

if [ "$SP_WP_INSTALL" = true ]; then
    if [ ! -e /var/www/html/index.php ] && [ ! -e /var/www/html/wp-includes/version.php ]; then
        log "Installing WordPress in /var/www/html..."
        wp core download --path="/var/www/html" --allow-root
    else
        log "WordPress already installed, skipping installation..."
    fi
fi

log "Resetting permissions..."
chown -R www-data:www-data /var/www/html

log "Starting supervisor..."
exec "$@"
